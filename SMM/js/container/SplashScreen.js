import React, {Component} from 'react'
import {
  View,
  StyleSheet,
  Text,
  Image
} from 'react-native'

export default class SplashScreen extends Component {

  render() {
    return (
      <View style = {styles.container}>
          <Image
            source = {require('../res/img/loading.gif')}
            style={{width: 100, height: 100}} />
          <Text style = {styles.textStyle}>
              Fetching your data, Please wait...
          </Text>
      </View>
    )
  }
}

var styles = StyleSheet.create({
  container : {justifyContent: 'center',
    alignItems: 'center',
    flex : 1,
    flexDirection : 'column',
  },
  textStyle : {
    color : 'black'
  }
})
