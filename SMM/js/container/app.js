import React, { Component } from 'react';
import {Router, Scene} from 'react-native-router-flux'
import {connect} from 'react-redux';

import SplashScreen from './SplashScreen'

export default class App extends Component {
  render() {
    return (
      <Router hideNavBar = {true}>
        <Scene key="SplashScreen" component = {SplashScreen} initial = {true}/>
      </Router>
    )
  }
}
