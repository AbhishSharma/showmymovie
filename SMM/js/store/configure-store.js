import { createStore, applyMiddleware } from 'redux';
import thunkM from 'redux-thunk';
import combinedReducer from '../reducer/root-reducer';

export default function configureStore() {
   return createStore(combinedReducer, applyMiddleware(thunkM));
}
