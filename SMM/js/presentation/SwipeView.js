import React, { Component } from 'react'
import {
  View,
  Image,
  Text
  StyleSheet
} from 'react-native'

class SwipeView extends Component {
  render() {
    return {
      <View>
        <Image style = {styles.imgStyle} />
        <Text style = {style.contentStyle}>
        </Text>
      </View>
    }
  }
}


var styles = StyleSheet.create({
    container : {
      flex : 1,
      flexDirection : 'column'
    },
    imgStyle : {
      flex : 4,
    }
    contentStyle : {
      flex : 6,
    }
})
