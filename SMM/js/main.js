import React, { Component } from 'react';

import App from './container/app'
import { Provider } from 'react-redux';
import configureStore from './store/configure-store'

const store = configureStore();

export default class Main extends Component {
    render () {
        return (
            <Provider store={store}>
                <App />
            </Provider>
        )
    }
}
