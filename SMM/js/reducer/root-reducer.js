import {combineReducers} from 'redux'

import moviesReducer from './movies'

var combinedReducer = combineReducers({moviesReducer})
export default combinedReducer;
