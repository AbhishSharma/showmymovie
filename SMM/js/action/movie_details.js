import {
  GET_DEFAULT_MOVIE,
  GET_REQUESTED_MOVIE,
  MOVIE_REQUEST_RECEIVED,
  MOVIE_RESPONSE_SUCCESS,
  MOVIE_RESPONSE_FAILURE
} from '../common/actions'

import movieServer from '../server/MovieApi'

let getMovie = (searchCriteria) => {
  dispatch(getMovieInitiaited())
  if (searchCriteria == 'undefined') {
    return movieServer.getDefaultMovie()
    .then((movies) => {
      dispatch(moviesSearchSuccess(movies));
    }).catch((error) => {
      dispatch(movieSearchFailed(error));
    })
  }
  return {
    type : GET_DEFAULT_MOVIE,

  }

  let getMovieInitiaited = () => {
    return {
      type : MOVIE_REQUEST_RECEIVED,
      showLoading : true
    }
  }

  let moviesSearchSuccess = (movies) => {
    return {
      type : MOVIE_RESPONSE_SUCCESS,
      showLoading : false,
      movies = movies,
      scene : "HomeScreen"
    }
  }

  let movieSearchFailed = (error) => {
    return {
      type : MOVIE_RESPONSE_FAILURE,
      showLoading : false,
      error : error
    }
  }
}
