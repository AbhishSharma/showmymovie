/**
 *  Created By :- Abhishek Sharma
 *  Dated : 23-August-2016
 */
SMM stands for Show My Movie.

Technology used

Technology Used         Used For          Version
-----------------------------------------------------
React native            UI component
Redux                   Navigation
Realm                   Persistence
Thunk                   Server call

Animation               https://github.com/oblador/react-native-animatable
Animated Icon            http://preloaders.net/en/circular

Server                  TMDB - http://docs.themoviedb.apiary.io/#
                        OMDB - http://www.omdbapi.com/
                        Mivie API - http://www.myapifilms.com/index.do

Package structure follows simple camel case strategy.

# All the folder name, which corresponds to package, will start with small letter
# All individual presentation component will start with capital letter.
# All container files, which are combination of various native as well as
presentation component will be small letter. Two words will be separated out by '-'
#
